from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.


def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/todo_list.html", context)


def todo_list_detail(request, id):
    todo_detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_detail": todo_detail,
    }
    return render(request, "todos/todo_detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)

        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)

    else:
        form = TodoListForm()
        context = {
            "form": form,
        }

        return render(request, "todos/create_todolist.html", context)


def todo_list_update(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=post)

        context = {
            "post": post,
            "form": form,
        }

        return render(request, "todos/update_todolist.html", context)


def todo_list_delete(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        post.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)

        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
        context = {
            "form": form,
        }

        return render(request, "todos/create_item.html", context)


def todo_item_update(request, id):
    post = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=post)

        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=post)
        context = {
            "post": post,
            "form": form,
        }
        return render(request, "todos/edit_item.html", context)
